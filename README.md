MANGA MANIA
===========

The manga reader with famous titles and unknown series.

License
=======

> **Manga Mania** is distributed individually under the terms of
> the BSD-2-Clause license that can be found in the LICENSE file.
> We encourage new contributors to distribute files under this license.

* Copyright (c) 2019 Carlos Donizete Froes [a.k.a coringao]
