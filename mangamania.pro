######################################################################
# Manga Mania is distributed under the terms of the BSD-2-Clause
# license that can be found in the LICENSE file.
######################################################################

TEMPLATE = app
TARGET = mangamania
INCLUDEPATH += .

QT += core gui widgets

# Output directory
OBJECTS_DIR += src
MOC_DIR += src
RCC_DIR += src
UI_DIR += src

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += src/book.h \
           src/bookloader.h \
           src/dialog.h \
           src/mainwindow.h \
           src/window.h
FORMS += src/dialog.ui src/mainwindow.ui src/window.ui
SOURCES += src/book.cpp \
           src/bookloader.cpp \
           src/dialog.cpp \
           src/main.cpp \
           src/mainwindow.cpp \
           src/window.cpp
RESOURCES += src/resource.qrc
