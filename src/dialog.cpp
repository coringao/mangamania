// Copyright (c) 2019-2020, Carlos Donizete Froes [a.k.a coringao].
// Use of this file is distributed under the license terms of BSD-2-Clause,
// which can be found in the LICENSE file.
#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>

mangamaniaDialog::mangamaniaDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mangamaniaDialog)
{
    ui->setupUi(this);
}

mangamaniaDialog::~mangamaniaDialog()
{
    delete ui;
}

QString mangamaniaDialog::getSeries(){
    return ui->seriesLineEdit->text();
}

QString mangamaniaDialog::getBookNumber(){
    return ui->bookNumberLineEdit->text();
}

QString mangamaniaDialog::getPath(){
    return ui->pathLineEdit->text();
}

void mangamaniaDialog::on_browseButton_clicked(){
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "/home",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    ui->pathLineEdit->setText(dir);
}
