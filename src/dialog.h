// Copyright (c) 2019-2020, Carlos Donizete Froes [a.k.a coringao].
// Use of this file is distributed under the license terms of BSD-2-Clause,
// which can be found in the LICENSE file.
#ifndef MANGAMANIADIALOG_H
#define MANGAMANIADIALOG_H

#include <QDialog>
#include <QString>
#include <QLineEdit>

namespace Ui {
class mangamaniaDialog;
}

class mangamaniaDialog : public QDialog
{
    Q_OBJECT

public:
    explicit mangamaniaDialog(QWidget *parent = 0);
    ~mangamaniaDialog();

    //variables for passing data to main window
    QString getSeries();
    QString getBookNumber();
    QString getPath();

private slots:
    void on_browseButton_clicked();

private:
    Ui::mangamaniaDialog *ui;

};

#endif // MANGAMANIADIALOG_H
