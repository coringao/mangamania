// Copyright (c) 2019-2020, Carlos Donizete Froes [a.k.a coringao].
// Use of this file is distributed under the license terms of BSD-2-Clause,
// which can be found in the LICENSE file.
#ifndef MANGAMANIAWINDOW_H
#define MANGAMANIAWINDOW_H
#include <QLineEdit>

#include <QWidget>

namespace Ui {
class mangamaniaWindow;
}

class mangamaniaWindow : public QWidget
{
    Q_OBJECT

public:
    explicit mangamaniaWindow(QWidget *parent = 0);
    ~mangamaniaWindow();

    //Elements
    QString seriesText;
    QString bookNumberText;
    QString pathText;

private slots:
    void on_pushButton_3_clicked();

private:
    Ui::mangamaniaWindow *ui;
};

#endif // MANGAMANIAWINDOW_H
