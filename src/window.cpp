// Copyright (c) 2019-2020, Carlos Donizete Froes [a.k.a coringao].
// Use of this file is distributed under the license terms of BSD-2-Clause,
// which can be found in the LICENSE file.
#include "window.h"
#include "ui_window.h"

mangamaniaWindow::mangamaniaWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mangamaniaWindow)
{
    ui->setupUi(this);

}

mangamaniaWindow::~mangamaniaWindow()
{
    delete ui;
}

void mangamaniaWindow::on_pushButton_3_clicked()
{
    bookNumberText = ui->bookNumberLineEdit->text();
    pathText = ui->pathLineEdit->text();
    seriesText = ui->seriesLineEdit->text();
}
