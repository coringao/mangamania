// Copyright (c) 2019-2020, Carlos Donizete Froes [a.k.a coringao].
// Use of this file is distributed under the license terms of BSD-2-Clause,
// which can be found in the LICENSE file.
#include "mainwindow.h"
#include "book.h"
#include "bookloader.h"
#include <QApplication>
#include <QDebug>
#include <QStyleFactory>
#include <QStandardPaths>


int main(int argc, char *argv[])
{
    //Create application and show main window
    QApplication app(argc, argv);
    app.setApplicationName("Manga Mania");
    app.setWindowIcon(QIcon(":/mangamania.png"));
    MainWindow w;
    w.show();
    return app.exec();

}
