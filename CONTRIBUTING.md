            .,',,,'.                               .,;,,'...            
           .c.    .,;,... ..';;;;;,,'  .',..    .,;,..     ..           
          ,;. ...    .od;';;'...        . .',;cxk:  .......:c.          
          ,l,......,;;'                       .';:c,.......,:.          
          .,......;;.                              .  ......,.          
          :l ....                                       ... ;'          
          co ..                                          .. oc          
          co..                                            . o:          
          .o'        .';:.                    .cll;.        :.          
           ox'...'''ckxoc,...              .....':oxc''....c:           
        ,ok00kdlccc::::ccclloodol,.....cdxdollccc:::::cclodOOkd:.       
       .OKKk'       .,:c:;.  .'lO0OOOO0Kk;.   ':c:;.       ;0KKK;       
        ;OKo      'ooc;,;lkx'  .dKOxdx00:  .ckx:,,;lol.    .kKKo.       
         c0d     .,.      .;o.  do.   ;O;  ;d'       ',    .d0x.        
          ck,                  ,x.     od.                 .xx.         
         .;dk,                ;k:      .xl.               .do.          
           .lxl:;;,,,,;;;:cccc;.         ,cccc::;;;;,,;;:cod;.          
             ,,.     .......    .:;;cc;;:;.   ......    .,,..           
               ':.      .......   .    ..   .......    '.               
                .;;,..ckdllllllool;.   .;loolllllloko::,                
                  .lxk0k.  ...   .cxl,oxc'.  ....  xKx:'                
                  ;K0k0o  ;llllc,. 'kKx. .:..lll;  oKk0O.               
                 cxlcxXl  ,;;:cllc. cK; ,ll..,,,'  oXkcox,              
                .X;  .Od  .......;. cK; ''...',,'  kk.  xK'             
                .Oo..:0: .lll:';;.  cK; .':llllll. o0:.'Ox.             
                 .00d0x  ....'..:l. cK; 'l:,'..... 'K0d0x               
                 .0d.do  '::;,..... cK; ....,,.:l, .xx.dd               
                 ,Kl.xc  ;llllllc,  cK; .;clc',cc,  oO.lk.              
                 :0;.Ol.......',;c. :K; .:,.. ......oK,:0:              
                 lk..lddxxdxxo:,.   cK;   .,:ldxdxxdol.,Kl              
                 dk:;;;;;::ccloodoc.lK:'coddollc::;;;;;lKd.             
                 dKKKKKKKKKKKK0OxkK00K00KOkOKKKKKKKKKKKKKx.             
                 :OkkxddoooooodxxO0KKKKK0OkxdoooooodxxkOO:              

How to contribute?
==================

Everyone is invited to contribute to the "Manga Mania" by submitting
bug reports, testing on different platforms, writing examples,
improving documentation, profiling and optimizing, helping newbies,
telling others about the software, etc.

**Submitting bugs**
-------------------

1. Make sure you have or create a GIT repository account.
2. Submit your issue (make sure the same issue does not exist yet).
3. Try to clearly describe the problem and include steps to reproduce
when it is an error.
4. Create a topic branch from which you want to base your work.
5. Name your affiliate with the type of problem you are correcting feat, task,
documents.
6. Avoid working directly at your main affiliate.

Thank you for considering to contribute to Manga Mania
======================================================

Contributions must be made under Manga Mania, under the terms of the
BSD-2-Clause license that can be found in the [LICENSE](LICENSE) file.

We encourage new contributors to distribute files under this license.
Source code and contact info at https://gitlab.com/coringao/mangamania

* Copyright (c) 2019 Carlos Donizete Froes [a.k.a coringao]
